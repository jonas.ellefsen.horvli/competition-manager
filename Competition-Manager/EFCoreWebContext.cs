﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    class EFCoreWebContext: DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<CompetitorSkills> CompetitorSkills { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\;Database=EFCTest;Trusted_Connection=true;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitorSkills>().HasKey(cs => new { cs.CompetitorID, cs.SkillID });
        }

    }
}
