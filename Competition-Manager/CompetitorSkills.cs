﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    class CompetitorSkills
    {
        public int SkillID { get; set; }
        public Skill Skill { get; set; }
        public int CompetitorID { get; set; }
        public Competitor Competitor { get; set; }
    }
}
