﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    class Coach
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? CompetitorID { get; set; }
        public Competitor Competitor { get; set; }
        public Coach()
        {

        }
    }
}
