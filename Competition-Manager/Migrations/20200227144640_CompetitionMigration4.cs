﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competition_Manager.Migrations
{
    public partial class CompetitionMigration4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamID",
                table: "Competitors");

            migrationBuilder.RenameColumn(
                name: "TeamID",
                table: "Competitors",
                newName: "TeamId");

            migrationBuilder.RenameIndex(
                name: "IX_Competitors_TeamID",
                table: "Competitors",
                newName: "IX_Competitors_TeamId");

            migrationBuilder.AddColumn<int>(
                name: "Division",
                table: "Teams",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Skills",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Skills", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CompetitorSkills",
                columns: table => new
                {
                    SkillID = table.Column<int>(nullable: false),
                    CompetitorID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompetitorSkills", x => new { x.CompetitorID, x.SkillID });
                    table.ForeignKey(
                        name: "FK_CompetitorSkills_Competitors_CompetitorID",
                        column: x => x.CompetitorID,
                        principalTable: "Competitors",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CompetitorSkills_Skills_SkillID",
                        column: x => x.SkillID,
                        principalTable: "Skills",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompetitorSkills_SkillID",
                table: "CompetitorSkills",
                column: "SkillID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors",
                column: "TeamId",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Teams_TeamId",
                table: "Competitors");

            migrationBuilder.DropTable(
                name: "CompetitorSkills");

            migrationBuilder.DropTable(
                name: "Skills");

            migrationBuilder.DropColumn(
                name: "Division",
                table: "Teams");

            migrationBuilder.RenameColumn(
                name: "TeamId",
                table: "Competitors",
                newName: "TeamID");

            migrationBuilder.RenameIndex(
                name: "IX_Competitors_TeamId",
                table: "Competitors",
                newName: "IX_Competitors_TeamID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Teams_TeamID",
                table: "Competitors",
                column: "TeamID",
                principalTable: "Teams",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
