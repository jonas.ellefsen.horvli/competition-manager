﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competition_Manager.Migrations
{
    public partial class CompetitionMigration3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches");

            migrationBuilder.DropIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches");

            migrationBuilder.AlterColumn<int>(
                name: "CompetitorID",
                table: "Coaches",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                unique: true,
                filter: "[CompetitorID] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                principalTable: "Competitors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches");

            migrationBuilder.DropIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches");

            migrationBuilder.AlterColumn<int>(
                name: "CompetitorID",
                table: "Coaches",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                principalTable: "Competitors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
