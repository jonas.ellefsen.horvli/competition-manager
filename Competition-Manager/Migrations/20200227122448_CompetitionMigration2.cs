﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Competition_Manager.Migrations
{
    public partial class CompetitionMigration2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitors_Coaches_TrainerID",
                table: "Competitors");

            migrationBuilder.DropIndex(
                name: "IX_Competitors_TrainerID",
                table: "Competitors");

            migrationBuilder.DropColumn(
                name: "TrainerID",
                table: "Competitors");

            migrationBuilder.AddColumn<int>(
                name: "CompetitorID",
                table: "Coaches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches",
                column: "CompetitorID",
                principalTable: "Competitors",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Coaches_Competitors_CompetitorID",
                table: "Coaches");

            migrationBuilder.DropIndex(
                name: "IX_Coaches_CompetitorID",
                table: "Coaches");

            migrationBuilder.DropColumn(
                name: "CompetitorID",
                table: "Coaches");

            migrationBuilder.AddColumn<int>(
                name: "TrainerID",
                table: "Competitors",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Competitors_TrainerID",
                table: "Competitors",
                column: "TrainerID");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitors_Coaches_TrainerID",
                table: "Competitors",
                column: "TrainerID",
                principalTable: "Coaches",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
