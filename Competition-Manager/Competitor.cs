﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    class Competitor
    {
        public int ID { get; private set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public Coach Trainer { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public ICollection<CompetitorSkills> competitorSkills { get; set; }


        public Competitor() { }

        public override string ToString()
        {
            return $"Id: {ID}\tName: {Name}\tAge: {Age}\tWeight: {Weight}kg\tHeight: {Height}cm";
        }
    }
}
