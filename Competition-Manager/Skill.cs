﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Competition_Manager
{
    class Skill
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<CompetitorSkills> CompetitorSkills { get; set; }

        public Skill() { }
    }
}
