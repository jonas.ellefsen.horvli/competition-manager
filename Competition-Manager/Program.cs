﻿using System;

namespace Competition_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            SeedCompetitors();
        }

        public static void SeedCompetitors()
        {
            using(EFCoreWebContext dbContext = new EFCoreWebContext())
            {
                dbContext.Competitors.Add(new Competitor() { Age = 20, Name = "Some Name", Height = 180, Weight = 80 });
                dbContext.Competitors.Add(new Competitor() { Age = 20, Name = "No Name", Height = 180, Weight = 80 });
                dbContext.Competitors.Add(new Competitor() { Age = 20, Name = "SOmething Name", Height = 190, Weight = 90 });
                dbContext.Competitors.Add(new Competitor() { Age = 20, Name = "Can't think of Name", Height = 200, Weight = 100 });

                dbContext.SaveChanges();

                Console.WriteLine("Competitors:");
                foreach (Competitor competitor in dbContext.Competitors)
                {
                    Console.WriteLine(competitor.ToString());
                }
            }
        }
    }
}
